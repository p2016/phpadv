<?php
namespace app\controllers;
use \framework\BaseController as BaseController;
use app\models\UserModel as UserModel;
use framework\FlashMessages as Mess;

class UserController extends BaseController{
	// o proprietate privata care contine modelul
	private $_model;

	// conectarea la model - daca este cazul
	public function __construct()
	{
		$this->_model = new UserModel;			
	}

	public function loginAction()
	{					
		if(!empty($_POST)){
			// database check					
			if($_POST['username'] == 'admin'){
				$this->redirect('index.php');
			}
		}
		$this->render('loginForm', array());
	}

	public function listAction()
	{
		$users = $this->_model->listUser();		
		$this->render('list', array('users' => $users));
	}

	public function editAction()
	{
		// method from parent Model class (must have id parameter in URL)
		$user = $this->_model->findByPk('users');
		if(isset($_POST['User'])){
			if($this->_model->update((int)$_GET['id'], $_POST['User'])){
				Mess::setMess('success', 'Update cu succes!');
				$this->redirect('index.php?c=user&a=list');				
			}			
		}

		$this->render('update', array('user' => $user));

	}

	public function addAction()
	{

	}

	public function deleteAction()
	{
		if(!empty($_POST['id'])){
			$this->_model->delete('users', $_POST['id']);
		}		
		$users = $this->_model->listUser();		
		$this->renderPartial('_list', array('users' => $users));
	}	
}