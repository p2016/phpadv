<!DOCTYPE html>
<html>
<head>
<title>Bloggers Green</title>
<meta charset="utf-8" />
<link href= "<?php echo 'public/theme/'.\framework\Framework::$params['theme']; ?>/css/default.css" rel="stylesheet" type="text/css" />
<script src="public/js/jquery-3.1.0.min.js"></script>
</head>
<body>
<div id="content">
  <div>
    <h1>Bloggersgreen</h1>
    <h2>Free CSS Template</h2>
  </div>
  <div id="menu">
    <ul>
      <li><a href="index.php">Home</a></li>
      <li><a href="index.php?c=index&amp;a=page&amp;view=despre">About us</a></li> 
      <li><a href="index.php?c=user&amp;a=list">Users</a></li>      
      <li><a href="index.php?c=user&amp;a=login">Login</a></li>      
    </ul>
  </div>
  <div id="left">
    <div>
      <?php        
        $messages = \framework\FlashMessages::getAllMess();
        if($messages){
          echo '<ul>';
          foreach ($messages as $message) {
            echo "<li>$message</li>";
          }
          echo '</ul>';
        }
      ?>      
    </div>
    <?php echo $content; ?>
  </div>
  <div id="right">
    <h2>Lorem Ipsum</h2>
    <p>Nunc pellentesque. Sed vestibulum blandit nisl. Quisque elementum convallis purus. Suspendisse potenti. Donec nulla est, laoreet quis, pellentesque in. <a href="#">More&#8230;</a></p>
    <h2>Ipsum Dolorem</h2>
    <ul>
      <li><a href="#">Sagittis Bibendum Erat</a></li>
      <li><a href="#">Malesuada Turpis</a></li>
      <li><a href="#">Quis Gravida Massa</a></li>
      <li><a href="#">Inerat Viverra Ornare</a></li>
    </ul>
  </div>
  <div>
    <p>&nbsp;</p>
  </div>
  <div id="right2">
    <h2>Recent Updates</h2>
    <p><strong>[06/09/2006]</strong> Etiam odio mi, suscipit et, rhoncus ac, lacinia, nisl. Aliquam gravida massa eu arcu. <a href="#">More&#8230;</a></p>
    <p><strong>[06/06/2006]</strong> Fusce mollis tristique sem. Sed eu eros imperdiet eros interdum blandit. Vivamus sagittis bibendum erat. Curabitur malesuada. <a href="#">More&#8230;</a></p>
    <p><strong>[06/03/2006]</strong> Nunc pellentesque. Sed vestibulum blandit nisl. Quisque elementum convallis purus. Suspendisse potenti. Donec nulla est, laoreet quis, pellentesque in. <a href="#">More&#8230;</a></p>
  </div>
  <div id="footer">
    <p>Copyright &copy; 2006 Sitename.com. Designed by <a href="http://www.freecsstemplates.org" class="link1">Free CSS Templates</a></p>
  </div>
</div>
</body>
</html>